package ch.hevs.Library;

import ch.hevs.errors.BusinessException;
import ch.hevs.errors.ErrorCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

public class ValidationHelperTest {
    /***
     * Test Validation Helper
     */
    @Test
    public void thresholdValidationTest() {
        BusinessException e;
        // Threshold can't be lower than 2
        e = assertThrows(BusinessException.class,
                new Executable() {
                    @Override
                    public void execute() throws Throwable {
                        ValidationHelper.thresholdValidation(1, 1);
                    }
                });
        assertEquals(ErrorCode.BAD_PARAMETER.getCode(), e.getErrorCode());

        // Threshold can't exceed total number of secret parts
        e = assertThrows(BusinessException.class,
                new Executable() {
                    @Override
                    public void execute() throws Throwable {
                        ValidationHelper.thresholdValidation(2, 1);
                    }
                });
        assertEquals(ErrorCode.BAD_PARAMETER.getCode(), e.getErrorCode());

        // Correct
        assertDoesNotThrow(() -> ValidationHelper.thresholdValidation(2, 2));
    }

    @Test
    public void noNullCheckTest() {
        BusinessException e;
        // noNullCheck for int
        e = assertThrows(BusinessException.class,
                new Executable() {
                    @Override
                    public void execute() throws Throwable {
                        ValidationHelper.noNullCheck(0, "Test value false");
                    }
                });
        assertEquals(ErrorCode.NO_VALUE.getCode(), e.getErrorCode());

        // Correct
        assertDoesNotThrow(() -> ValidationHelper.noNullCheck(2, "Test value correct"));

        // noNullCheck for string
        e = assertThrows(BusinessException.class,
                new Executable() {
                    @Override
                    public void execute() throws Throwable {
                        ValidationHelper.noNullCheck((String) null, "Test value false");
                    }
                });
        assertEquals(ErrorCode.NO_VALUE.getCode(), e.getErrorCode());

        // Correct
        assertDoesNotThrow(() -> ValidationHelper.noNullCheck("Test", "Test value correct"));

        // noNullCheck for String array
        e = assertThrows(BusinessException.class,
                new Executable() {
                    @Override
                    public void execute() throws Throwable {
                        ValidationHelper.noNullCheck((String[]) null, "Test value false");
                    }
                });
        assertEquals(ErrorCode.NO_VALUE.getCode(), e.getErrorCode());

        // Correct
        assertDoesNotThrow(() -> ValidationHelper.noNullCheck("Test", "Test value correct"));

    }

    @Test
    public void sizeValidationTest() {
        BusinessException e;
        // Threshold can't be lower than 2
        e = assertThrows(BusinessException.class,
                new Executable() {
                    @Override
                    public void execute() throws Throwable {
                        ValidationHelper.sizeValidation(1);
                    }
                });
        assertEquals(ErrorCode.BAD_PARAMETER.getCode(), e.getErrorCode());

        // Threshold can't exceed total number of secret parts
        e = assertThrows(BusinessException.class,
                new Executable() {
                    @Override
                    public void execute() throws Throwable {
                        ValidationHelper.sizeValidation(45);
                    }
                });
        assertEquals(ErrorCode.BAD_PARAMETER.getCode(), e.getErrorCode());

        // Correct
        assertDoesNotThrow(() -> ValidationHelper.sizeValidation(16));
        assertDoesNotThrow(() -> ValidationHelper.sizeValidation(24));
        assertDoesNotThrow(() -> ValidationHelper.sizeValidation(32));
    }

}