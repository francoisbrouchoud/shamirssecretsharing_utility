package ch.hevs.Library;
import ch.hevs.Class.Coordinate;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

public class MathHelperTest {
    @Test
    public void testFindY(){
        BigInteger tabCoef[] = {BigInteger.ONE, BigInteger.TWO, BigInteger.valueOf(3)};
        testFindY(tabCoef, BigInteger.valueOf(1), BigInteger.valueOf(5), BigInteger.valueOf(1));

    }

    private void testFindY(BigInteger[] tabCoef, BigInteger x, BigInteger mod, BigInteger expectedY) {
        assertEquals(expectedY, MathHelper.findY(tabCoef, x, mod));
    }

    @Test
    public void testFindSecret(){
        //Case 1
        BigInteger expected = BigInteger.valueOf(9);
        BigInteger modulo = BigInteger.valueOf(17);

        Coordinate coor1 = new Coordinate(BigInteger.valueOf(6),BigInteger.valueOf(15),8);
        Coordinate coor2 = new Coordinate(BigInteger.TWO,BigInteger.TEN,8);
        Coordinate coor3 = new Coordinate(BigInteger.valueOf(3),BigInteger.valueOf(8),8);
        Coordinate coor4 = new Coordinate(BigInteger.valueOf(4),BigInteger.valueOf(5),8);
        Coordinate coor5 = new Coordinate(BigInteger.ONE,BigInteger.TWO,8);
        Coordinate coor6 = new Coordinate(BigInteger.valueOf(5),BigInteger.TEN,8);
        Coordinate coor7 = new Coordinate(BigInteger.valueOf(7),BigInteger.valueOf(12),8);
        Coordinate coor8 = new Coordinate(BigInteger.valueOf(8),BigInteger.valueOf(10),8);

        Coordinate[] coordinate11 = { coor1, coor2, coor3, coor4};
        testFindSecret(coordinate11, modulo, expected);

        Coordinate[] coordinate12 = { coor1, coor2, coor3, coor5};
        testFindSecret(coordinate12, modulo, expected);

        Coordinate[] coordinate13 = { coor5, coor6, coor7, coor8};
        testFindSecret(coordinate13, modulo, expected);

        //Case 2
        expected = BigInteger.valueOf(17);
        modulo = BigInteger.valueOf(19);

        coor1 = new Coordinate(BigInteger.ONE,BigInteger.valueOf(9),8);
        coor2 = new Coordinate(BigInteger.valueOf(3),BigInteger.valueOf(3),8);
        coor3 = new Coordinate(BigInteger.valueOf(5),BigInteger.valueOf(17),8);
        coor4 = new Coordinate(BigInteger.valueOf(7),BigInteger.valueOf(2),8);
        coor5 = new Coordinate(BigInteger.valueOf(9),BigInteger.valueOf(4),8);
        coor6 = new Coordinate(BigInteger.valueOf(11),BigInteger.valueOf(12),8);

        Coordinate[] coordinate21 = { coor1, coor2, coor3, coor4};
        testFindSecret(coordinate21, modulo, expected);

        Coordinate[] coordinate22 = { coor1, coor4, coor3, coor5};
        testFindSecret(coordinate22, modulo, expected);

        Coordinate[] coordinate23 = { coor5, coor6, coor1, coor2};
        testFindSecret(coordinate23, modulo, expected);

    }

    private void testFindSecret(Coordinate[] coordinates, BigInteger modulo, BigInteger expectedSecret){
        assertEquals(expectedSecret, MathHelper.findSecret(coordinates,modulo));
    }


    @Test
    public void testInverseMultiplicative(){
        testInverseMultiplicative(BigInteger.valueOf(45),BigInteger.valueOf(18),BigInteger.valueOf(-2));
        testInverseMultiplicative(BigInteger.valueOf(5),BigInteger.valueOf(3),BigInteger.valueOf(2));
        testInverseMultiplicative(BigInteger.valueOf(59),BigInteger.valueOf(12),BigInteger.valueOf(5));
        testInverseMultiplicative(BigInteger.valueOf(17),BigInteger.valueOf(-1),BigInteger.valueOf(1));
    }

    private void testInverseMultiplicative(BigInteger value1, BigInteger value2, BigInteger expectedResult){
        assertEquals(expectedResult, MathHelper.getMultiplicativeInverse(value1, value2));
    }

    @Test
    public void testComputeModulo(){
        BigInteger expectedModuloFor4 = new BigInteger("4294967311");
        BigInteger expectedModuloFor16 = new BigInteger("340282366920938463463374607431768211507");
        BigInteger expectedModuloFor24 = new BigInteger("6277101735386680763835789423207666416102355444464034513029");
        BigInteger expectedModuloFor32 = new BigInteger("115792089237316195423570985008687907853269984665640564039457584007913129640233");

        assertEquals(expectedModuloFor4, MathHelper.computeModulo(4));
        assertEquals(expectedModuloFor16, MathHelper.computeModulo(16));
        assertEquals(expectedModuloFor24, MathHelper.computeModulo(24));
        assertEquals(expectedModuloFor32, MathHelper.computeModulo(32));
    }

    @Test
    public void testGenerateCoef(){
        int threshold = 4;
        int nbByte = 3;
        BigInteger[] coefTest = MathHelper.generateCoefficient(threshold, nbByte);

        assertEquals(threshold, coefTest.length);

        BigInteger valueTest = BigInteger.TWO.pow(nbByte*8);
        for (int i = 0; i < threshold; i++) {
            assertEquals(-1, coefTest[i].compareTo(valueTest));
        }
    }
}
