package ch.hevs.Library;
import ch.hevs.Class.Coordinate;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;

import static org.junit.Assert.assertEquals;

public class FileHelperTest {
    /***
     * Test FileHelper
     */

    @Test
    public void writeReadKeyTest() {
        int numberFiles=10;
        String path = "secretParts\\";
        Coordinate[] coordinateOrigin = new Coordinate[numberFiles];
        String[] files = new String[numberFiles];
        for (int i = 0; i < numberFiles; i++) {
            SecureRandom rand = new SecureRandom();
            BigInteger nbRand = new BigInteger(rand.generateSeed(24));
            coordinateOrigin[i] = new Coordinate(BigInteger.valueOf(i), nbRand, 24);
            files[i] = "testCoor" + i + ".json";
            try {
                FileHelper.writeKey(coordinateOrigin[i], files[i]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         *  Path to read each part of secret
         */
        for (int i = 0; i < numberFiles; i++) {
            files[i] =  path + files[i];
        }

        Coordinate[] coordinates = FileHelper.readKey(files);
        for (int i = 0; i < numberFiles; i++) {
            assertEquals(coordinateOrigin[i], coordinates[i]);
        }
    }
}