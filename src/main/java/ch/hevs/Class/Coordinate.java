package ch.hevs.Class;

import java.math.BigInteger;

public class Coordinate {
    private BigInteger x;
    private BigInteger y;
    private int size;

    public Coordinate() {
    }

    public Coordinate(BigInteger x, BigInteger y, int size) {
        setX(x);
        setY(y);
        setSize(size);
    }

    public BigInteger getX() {
        return x;
    }

    public void setX(BigInteger x) {
        this.x = x;
    }

    public BigInteger getY() {
        return y;
    }

    public void setY(BigInteger y) {
        this.y = y;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coordinate)) return false;
        Coordinate that = (Coordinate) o;
        return x.equals(that.x) && y.equals(that.y);
    }
}
