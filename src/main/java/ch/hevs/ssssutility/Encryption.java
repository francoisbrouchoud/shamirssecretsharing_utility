package ch.hevs.ssssutility;

import ch.hevs.Class.Coordinate;
import ch.hevs.Library.FileEncryption;
import ch.hevs.Library.FileHelper;
import ch.hevs.Library.MathHelper;
import ch.hevs.Library.ValidationHelper;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

import java.io.File;
import java.math.BigInteger;
import java.security.Security;
import java.util.Arrays;
import java.util.List;


@Command(name = "Encryption")
public class Encryption implements Runnable{

    private final List<String> encryptValues = Arrays.asList("Encrypt", "encrypt", "E", "e", "En", "en");
    private final List<String> decryptValues = Arrays.asList("Decrypt", "decrypt", "D", "d", "De", "de");

    @Parameters(paramLabel = "Message", description = "Path of the file to transform", index = "0")
    private String filePathMessage;

    @Parameters(paramLabel = "Operation", description = "Encrypt to encrypt the file, Decrypt to decrypt the file", index = "1")
    private String operation;

    @Parameters(paramLabel = "Parts", description = "File's path of the secret's part", index = "2..*")
    private String[] secretPartsFilesPaths;

    public static void main(String[] args) {
        int exitCode = new CommandLine(new Encryption()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public void run() {
        try{
            // Check param
            ValidationHelper.noNullCheck(secretPartsFilesPaths, "secretPartsFilesPaths");
            ValidationHelper.noNullCheck(filePathMessage, "filePathMessage");
            ValidationHelper.noNullCheck(operation, "operation");

            // Build secret
            Coordinate[] Coordinates = FileHelper.readKey(secretPartsFilesPaths);
            BigInteger modulo = MathHelper.computeModulo(Coordinates[0].getSize());
            BigInteger key = MathHelper.findSecret(Coordinates, modulo);

            byte[] keyArray = key.toByteArray();

            // Remove signbit
            if (keyArray[0] == 0) {
                byte[] tmp = new byte[keyArray.length - 1];
                System.arraycopy(keyArray, 1, tmp, 0, tmp.length);
                keyArray = tmp;
            }

            // Encrypt or decrypt file
            if(encryptValues.contains(operation)){
                System.out.println("Encrypting...");
                Security.addProvider(new BouncyCastleProvider());
                File input = new File(filePathMessage);
                String fileNameEncrypted = determineFileName(filePathMessage, operation);
                File output = new File(fileNameEncrypted);
                FileEncryption.encrypt(keyArray, input, output);

                System.out.println("Encrypted!");
            }else if(decryptValues.contains(operation)){
                System.out.println("Decrypting...");
                Security.addProvider(new BouncyCastleProvider());
                File input = new File(filePathMessage);
                String fileNameDecrypted = determineFileName(filePathMessage, operation);
                File output = new File(fileNameDecrypted);
                FileEncryption.decrypt(keyArray, input, output);

                System.out.println("Decrypted!");
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Suffix name with operation to determine if file is encrypted or not
     * @param filePathMessage
     * @param operation
     * @return fileName
     */
    private String determineFileName(String filePathMessage, String operation) {
        String extension = filePathMessage.substring(filePathMessage.lastIndexOf('.'));
        String fileName = filePathMessage.substring(0, filePathMessage.length()-extension.length());

        return fileName + "-" + operation + extension;
    }

}
