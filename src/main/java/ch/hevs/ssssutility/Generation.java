package ch.hevs.ssssutility;

import ch.hevs.Class.Coordinate;
import ch.hevs.Library.FileHelper;
import ch.hevs.Library.MathHelper;
import ch.hevs.Library.ValidationHelper;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

import java.io.IOException;
import java.math.BigInteger;

@Command(name="Generation")
public class Generation implements Runnable{

    @Parameters(paramLabel = "secretSize", description = "Size of the secret (number of bytes)")
    private int secretSize;

    @Parameters(paramLabel = "threshold", description = "Minimum number of parts to reconstruct the secret")
    private int threshold;

    @Parameters(paramLabel = "secretParts", description = "Number of total parts to create")
    private int secretParts;

    public static void main(String[] args) {
        int exitCode = new CommandLine(new Generation()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public void run() {
        try{
            // Check param
            ValidationHelper.noNullCheck(secretParts, "secretParts");
            ValidationHelper.noNullCheck(threshold, "threshold");
            ValidationHelper.noNullCheck(secretSize, "secretSize");
            ValidationHelper.thresholdValidation(threshold,secretParts);
            ValidationHelper.sizeValidation(secretSize);

            System.out.println("Generating...");
            // generate keys
            generate(secretSize, threshold, secretParts);
            System.out.println("Generated!");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Generate secret
     * @param secretSize number of byte
     * @param threshold number of min key required
     * @param secretParts number of part asked
     * @throws IOException
     */
    private void generate (int secretSize, int threshold, int secretParts) throws IOException {
       BigInteger modulo = MathHelper.computeModulo(secretSize);
       BigInteger [] coefficients = MathHelper.generateCoefficient(threshold, secretSize);
        for (int i = 1; i <= secretParts; i++) {
            BigInteger x = BigInteger.valueOf(i);
            BigInteger y = MathHelper.findY(coefficients, x, modulo);
            Coordinate Coordinate = new Coordinate(x,y,secretSize);
            FileHelper.writeKey(Coordinate, "key" + i + ".json");
        }
    }
}
