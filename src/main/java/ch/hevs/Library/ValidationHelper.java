package ch.hevs.Library;

import ch.hevs.errors.BusinessException;
import ch.hevs.errors.ErrorCode;

public class ValidationHelper {

    public static void thresholdValidation(int threshold, int secretParts) throws BusinessException {
        if (threshold > secretParts) {
            throw new BusinessException("Threshold can't exceed total number of secret parts", ErrorCode.BAD_PARAMETER);
        }
        if (threshold < 2) {
            throw new BusinessException("Threshold can't be lower than 2", ErrorCode.BAD_PARAMETER);
        }
    }

    public static void noNullCheck(int value, String name) throws BusinessException {
        if (value == 0) {
            throw new BusinessException("No " + name + " provided", ErrorCode.NO_VALUE);
        }
    }

    public static void noNullCheck(String value, String name) throws BusinessException {
        if (value == null) {
            throw new BusinessException("No" + name + " provided", ErrorCode.NO_VALUE);
        }
    }

    public static void noNullCheck(String[] value, String name) throws BusinessException {
        if (value == null) {
            throw new BusinessException("No" + name + " provided", ErrorCode.NO_VALUE);
        }
    }

    public static void sizeValidation(int secretSize) throws BusinessException {
        if (secretSize != 16 && secretSize != 24 && secretSize != 32) {
            throw new BusinessException("Secret size must be 16, 24 or 32 bytes only", ErrorCode.BAD_PARAMETER);
        }
    }
}
