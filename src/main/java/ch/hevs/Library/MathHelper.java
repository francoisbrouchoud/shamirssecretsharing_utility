package ch.hevs.Library;

import ch.hevs.Class.Coordinate;

import java.math.BigInteger;
import java.security.SecureRandom;

public class MathHelper {

    /**
     * Compute line value
     *
     * @param x             value x
     * @param x0            value x for y at 1
     * @param allOtherPoint all points
     * @param mod           limit for arithmetic
     * @return find y for a part of calcul
     */
    private static BigInteger lineCompute(BigInteger x, BigInteger x0, Coordinate[] allOtherPoint, BigInteger mod) {
        BigInteger y = BigInteger.ONE;
        for (int i = 0; i < allOtherPoint.length; i++) {
            if (x0 != allOtherPoint[i].getX()) {
                BigInteger div = x.subtract(allOtherPoint[i].getX());
                // We need positive number to find multiplicative inverse  -> with module it works
                BigInteger divider = x0.subtract(allOtherPoint[i].getX()).mod(mod);
                BigInteger multiplicativeInverse = getMultiplicativeInverse(mod, divider);
                y = y.multiply(div.multiply(multiplicativeInverse)).mod(mod);
            }
        }
        return y;
    }

    /**
     * Find the key
     *
     * @param tablePoint All points
     * @param mod        limit for arithmetic
     * @return value y for f(0)
     */
    public static BigInteger findSecret(Coordinate[] tablePoint, BigInteger mod) {
        BigInteger result = BigInteger.ZERO;
        for (int i = 0; i < tablePoint.length; i++) {
            BigInteger temp = lineCompute(BigInteger.ZERO, tablePoint[i].getX(), tablePoint, mod);
            result = result.add(tablePoint[i].getY().multiply(temp));
        }

        return result.mod(mod);
    }

    /**
     * Find y value With Coefficient and a x value
     * ax^2 + bx + c = c + (b (x * + a*(x)) -> ((a) * x + b) * x + c
     * @param coef a, b, c, ... value of equation
     * @param x the value of function
     * @param mod limit for arithmetic
     * @return the value of y
     */
    public static BigInteger findY(BigInteger[] coef, BigInteger x, BigInteger mod) {
        BigInteger result = coef[0];
        for (int i = 1; i < coef.length; i++) {
            result = result.multiply(x).add(coef[i]).mod(mod);
        }
        return result;
    }

    /**
     * Find the first prime number with max number of byte
     * @param nbBytes number of byte
     * @return a prime number
     */
    public static BigInteger computeModulo(int nbBytes) {
        return BigInteger.TWO.pow(nbBytes * 8).nextProbablePrime();
    }

    /**
     * Generation of coefficients
     * @param threshold number of coef need
     * @param nbByte number of byte
     * @return list of coef
     */
    public static BigInteger[] generateCoefficient(int threshold, int nbByte) {
        BigInteger[] coef = new BigInteger[threshold];
        for (int i = 0; i < threshold; i++) {
            //secure random
            SecureRandom rand = new SecureRandom();
            coef[i] = new BigInteger(rand.generateSeed(nbByte));
        }
        return coef;
    }

    /**
     * Extended Euclide
     * Find the multiplicative inverse
     *
     * @param a Modulo value
     * @param b Value for which we are looking for the multiplicative inverse
     * @return y multiplicative inverse from b with a
     */

    public static BigInteger getMultiplicativeInverse(BigInteger a, BigInteger b) {
        BigInteger rPrevious = a;
        BigInteger rCourant = b;
        BigInteger yPrevious = BigInteger.ZERO;
        BigInteger yCourant = BigInteger.ONE;

        while (rCourant != BigInteger.ZERO) {
            BigInteger[] divide = rPrevious.divideAndRemainder(rCourant);
            rPrevious = rCourant;
            rCourant = divide[1];

            BigInteger yTemp = yPrevious.subtract(yCourant.multiply(divide[0]));
            yPrevious = yCourant;
            yCourant = yTemp;
        }

        return yPrevious;

    }
}
