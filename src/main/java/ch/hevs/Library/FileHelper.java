package ch.hevs.Library;

import ch.hevs.Class.Coordinate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

public class FileHelper {
    static final String SECRET_PARTS_FOLDER = "secretParts";

    /**
     * Serialize Coordinate (Secret Part) in a File
     * @param coordinate
     * @param secretPartFileName
     * @throws IOException
     */
    public static void writeKey(Coordinate coordinate, String secretPartFileName) throws IOException {
       
        File folder = new File(SECRET_PARTS_FOLDER);
        if(!folder.exists()){
            folder.mkdir();
        }
        File fileKey = new File(folder, secretPartFileName);

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(fileKey, coordinate);
    }


    /**
     * Deserialize Secret Parts and return a coordinates table
     * @param secretPartsFilesName
     * @return coordinates
     */
    public static Coordinate[] readKey(String[] secretPartsFilesName){
        ObjectMapper mapper = new ObjectMapper();
        Coordinate coordinates[] = new Coordinate[secretPartsFilesName.length];

        for (int i = 0; i < coordinates.length; i++) {
            try {
                coordinates[i] = mapper.readValue(new File(secretPartsFilesName[i]), Coordinate.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return coordinates;
    }
}
