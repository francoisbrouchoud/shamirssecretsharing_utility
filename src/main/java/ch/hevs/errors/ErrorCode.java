package ch.hevs.errors;

public enum ErrorCode {

    NO_VALUE(100),
    BAD_PARAMETER(200),
    CRYPTO_PROVIDER_ERROR(300),
    IO_ERROR(400),
    BAD_PADDING_ERROR(500),
    ILLEGAL_BLOCK_SIZE_ERROR(600),
    CIPHERTEXT_ERROR(700);

    private final int code;

    ErrorCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
