# Project Shamir's Secret Sharing Utility

**Auteurs** : Céline Vialard, François Brouchoud et Luca Del Buono

Ce projet présente un outil Java en ligne de commande qui permet la génération d'un secret de Shamir et de chiffrer et déchiffrer un document.
Il a été développé dans le cadre du projet du cours de Mathématiques 632-2 de la HES-SO Valais.

## 1. Utilisation du projet 

### Création du .jar
Dans l'onglet Maven Lifecycle, il faut lancer _package_ et _compile_ ou en ligne de commande `mvn package compile`.

### Génération
Permet de générer les parts du secret.

**Paramètres :**

- secretSize : taille du secret (en nombre de bytes)
- threshold : nombre de parts minimum à réunir pour déchiffrer le secret
- secretParts : nombre de parts totales du secret

Exemple : `java -jar generation-1.0-jar-with-dependencies.jar 16 4 10`

### Chiffrement - Déchiffrement
Permet de chiffrer et déchiffrer un document.

**Paramètres :**

- filePathMessage : chemin vers le fichier à transformer
- operation : opération à faire (**E**ncrypt : chiffrement ; **D**ecrypt : déchiffrement)
- secretPartsFilesPaths : tableau de chaînes de caractère contenant les chemins vers les fichiers des parts du secret

Avant de lancer les instructions dans l'interpréteur de commande, se positionner dans le dossier contenant les .jar. Par défaut, après avoir compilé le code, ils se trouvent dans le dossier _target_.

Exemple chiffrement : `java -jar .\encryption-1.0-jar-with-dependencies.jar ..\sampleFile.txt E .\secretParts\key1.json .\secretParts\key2.json .\secretParts\key3.json .\secretParts\key4.json`

Exemple déchiffrement : `java -jar .\encryption-1.0-jar-with-dependencies.jar ..\sampleFile-E.txt D .\secretParts\key1.json .\secretParts\key2.json .\secretParts\key3.json .\secretParts\key4.json`


## 2. Description des algorithmes  

### Find Secret
Retrouve la valeur de Y pour f(0) grâce à un nombre de coordonnées passées en paramètre.
Pour ce faire, nous calculons pour chaque point Y passé à 1, la ligne avec les autres points dont le Y est à 0. Ensuite, nous additionnons chacune de ces lignes en multipliant par la valeur de Y dont le point est passé à 1.

### Find Y
Trouve la valeur Y avec les coefficients et la valeur de X en utilisant le principe de calcul suivant : ((a) * x + b) * x + c.

### Compute Modulo
Trouve le prochain nombre premier d'un nombre de byte défini.

### Generate Coefficient
Génération aléatoire des coefficients.

### Get Multiplicative Inverse
Mise en place de l'algorithme d'Euclide étendu afin de retrouver l'inverse multiplicatif d'un nombre. Les nombres passés à cet algorithme doivent être obligatoirement positifs.
En utilisant le calcul pour retrouve le plus grand diviseur commun, nous pouvons ainsi trouver une valeur Y qui correspondra à l'inverse multiplicatif. Y<sub>n+1</sub> = Y<sub>n-1</sub>-(Y<sub>n</sub>*Q<sub>n</sub>)


## 3. Diagrammes

<img src="Class_Diagram.png" alt="class diagram" width="70%"  />

<img src="Generation_Diagram.png" alt="generation diagram" width="70%" />

<img src="Encryption_Diagram.png" alt="encryption diagram" width="70%" />

